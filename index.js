
const FIRST_NAME = "ILIE-ADRIAN";
const LAST_NAME = "LUNGU";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function initCaching() {
   var empty={};
   var cache={};
   cache.pageAccessCounter=function(websiteName='home')
   {
       websiteName=websiteName.toLowerCase();
       if(empty[websiteName]===undefined)
       empty[websiteName]=1;
       else
       empty[websiteName]++;
   }
   cache.getCache=function()
   {
       return empty;
   }
   return cache;
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

